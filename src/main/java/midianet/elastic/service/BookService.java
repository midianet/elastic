package midianet.elastic.service;

import midianet.elastic.document.Book;
import midianet.elastic.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {

    @Autowired
    private BookRepository repository;

    public Book save(Book book){
        return repository.save(book);
    }

    public void delete(Book book){
        repository.delete(book);
    }

    public Book findOne(String id){
        return repository.findById(id).get();
    }

    public Iterable<Book> findAll(){
        return  repository.findAll();
    }

    public Page<Book> findByAuthor(String author, PageRequest pageRequest){
        return repository.findByAuthor(author,pageRequest);
    }

    public List<Book> findByTitle(String title){
        return repository.findByTitle(title);
    }

}
